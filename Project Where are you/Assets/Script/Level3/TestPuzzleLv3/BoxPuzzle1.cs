﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxPuzzle1 : MonoBehaviour
{
    public bool onTriggerBox1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()


    {
        if(onTriggerBox1){

           CompletePuzzleLv3.onTriggerBox_1 = true;
        }
        
    }



    void OnTriggerEnter (Collider target) {
        if (target.tag == "Box1") {
            onTriggerBox1 = true;
        }
    }
    
    void OnTriggerExit (Collider target) {
        if (target.tag == "Box1") {
            onTriggerBox1 = false;
        }
    }
}



