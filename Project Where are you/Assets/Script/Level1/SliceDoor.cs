﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceDoor : MonoBehaviour {
    [SerializeField]
    GameObject door;

    public bool onTrigger;
    //bool isOpened = false;
    public bool doorOpen;
    public bool doorClose;
    public Transform doorHinge;

    private Vector3 startPos;
    private Vector3 endPos;
    private float distance = 4f;
    private float leapTime = 10f;
    private float currentLeapTime = 0;

    // Start is called before the first frame update
    void Start () {

        startPos = door.transform.position;
        endPos = door.transform.position + Vector3.right * distance;

    }

    // Update is called once per frame
    void Update () {
        if (onTrigger) {

            if (doorClose) {
                if (Input.GetKeyDown (KeyCode.E)) {
                    doorOpen = true;
                    doorClose = false;
                }
            } else {
                if (Input.GetKeyDown (KeyCode.E)) {
                    doorOpen = false;
                    doorClose = true;
                }
            }
        }

        if (doorOpen) {
            /*var newRot = Quaternion.RotateTowards (doorHinge.rotation, Quaternion.Euler (0.0f, -90.0f, 0.0f), Time.deltaTime * 250);
            doorHinge.rotation = newRot;*/

            currentLeapTime += Time.deltaTime;
            if(currentLeapTime >= leapTime){
                currentLeapTime = leapTime;
            }

            float Perc = currentLeapTime/leapTime;
            door.transform.position = Vector3.Lerp(startPos,endPos,Perc);


            

        }
        if (doorClose)

        {
            //var newRot = Quaternion.RotateTowards (doorHinge.rotation, Quaternion.Euler (0.0f, 0.0f, 0.0f), Time.deltaTime * 250);
            //doorHinge.rotation = newRot;

           currentLeapTime -= Time.deltaTime;
            if(currentLeapTime >= leapTime){
                currentLeapTime = leapTime;
            }

            float Perc = currentLeapTime/leapTime;
            door.transform.position = Vector3.Lerp(startPos,endPos,Perc);


        }

    }

    void OnTriggerEnter (Collider cal) {
        /*if(!isOpened){

            isOpened = true;
            door.transform.position += new Vector3 (0,0,4);

        }*/

        onTrigger = true;

    }

    void OnTriggerExit (Collider other) {
        onTrigger = false;

    }

    void OnGUI () {
        if (onTrigger) {

            if (doorOpen) {
                GUI.Box (new Rect (600, 500, 250, 26), "Press E to close");
            }
        }

        if (onTrigger) {

            if (doorClose) {
                GUI.Box (new Rect (600, 500, 250, 26), "Press E to Open");
            }
        }

    }
}