﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SignTrigger : MonoBehaviour
{
    public GameObject dialogBox;
    public GameObject Interacttext;
    public TextMeshProUGUI dialogText;
    public string dialog;
    public bool dialogActive;
    public bool Ondialog = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && dialogActive)
        {
            if (dialogBox.activeInHierarchy)
            {
                
                dialogBox.SetActive(false);
            }
            else
            {
                Interacttext.SetActive(false);
                dialogBox.SetActive(true);
                dialogText.text = dialog;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Player" )
        {
            Interacttext.SetActive(true);
            dialogActive = true;
           
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            Interacttext.SetActive(false);
            dialogActive = false;
            dialogBox.SetActive(false);
            
        }
    }
    
   
    //private void OnGUI()
   // {
        //if (!dialogActive)
        //{
            //if (Ondialog)
            //{
          //      GUI.Box(new Rect(340, 380, 200, 25), "Press 'E' to Read the Sign");
        //    }
      //  }
    //}
}
