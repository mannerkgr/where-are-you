﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCharacter : MonoBehaviour
{
    [SerializeField]
    public float _movementStep;
    public bool IsOnGround = true;
    public Rigidbody rb;
    Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
       
        //if (Input.GetKey(KeyCode.W))
        //{
        //    rb.AddRelativeForce(Vector3.forward * _movementStep);
        //}
        //if (Input.GetKey(KeyCode.S))
        //{
        //    rb.AddRelativeForce(Vector3.forward * -_movementStep);
        //}

         if (Input.GetKey(KeyCode.W))
         {
            this.transform.Translate(0, 0, _movementStep * Time.deltaTime);
         }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            this.transform.Translate(0, 0, _movementStep * 1.32f * Time.deltaTime);
        }
        else
        {

        }
        if(Input.GetButtonDown("Jump")&& IsOnGround)
        {
            rb.AddForce(new Vector3(0, 7, 0), ForceMode.Impulse);
            IsOnGround = false;
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            IsOnGround = true;
        }
    }


}
