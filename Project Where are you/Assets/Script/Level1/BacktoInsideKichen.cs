﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BacktoInsideKichen : MonoBehaviour
{
    public bool inTrigger;
    public void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
        
    }
    public void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }
    void OnGUI()
    {
        if (inTrigger)
        {
            GUI.Box(new Rect(600, 500, 250, 26), "Press E to go back to Kitchen Room");
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (inTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                SceneManager.UnloadSceneAsync("Level2");

                SceneManager.LoadScene("InsideDone");
            }
        }


    }
}