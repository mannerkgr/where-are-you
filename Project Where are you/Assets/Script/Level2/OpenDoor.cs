﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField]
    GameObject door;
    
    public bool onTrigger;
    //bool isOpened = false;
    public bool doorOpen;
    public Transform doorHinge;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(Input.GetKeyDown(KeyCode.E))
        {
            doorOpen = true;
        }
 
        if(doorOpen)
        {
            var newRot = Quaternion.RotateTowards(doorHinge.rotation, Quaternion.Euler(0.0f, -90.0f, 0.0f), Time.deltaTime * 250);
            doorHinge.rotation = newRot;
        }
        
    }






void OnTriggerEnter (Collider cal)
{
    /*if(!isOpened){

        isOpened = true;
        door.transform.position += new Vector3 (0,0,4);

    }*/

      onTrigger = true;
    
    
}


  void OnTriggerExit(Collider other)
    {
        onTrigger = false;
        
        
    }

void OnGUI()
    {
        if(!doorOpen)
        {
            if(onTrigger)
            {
                GUI.Box(new Rect(340, 380, 200, 25), "Press 'E' to open The Room");
 
                
            }
        }
 
    }


}

