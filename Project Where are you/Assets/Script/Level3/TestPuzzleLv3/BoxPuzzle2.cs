﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxPuzzle2 : MonoBehaviour
{
    public bool onTriggerBox2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(onTriggerBox2){

           CompletePuzzleLv3.onTriggerBox_2 = true;
        }
    }



    void OnTriggerEnter (Collider target) {
        if (target.tag == "Box2") {
            onTriggerBox2 = true;
        }
    }
    
    void OnTriggerExit (Collider target) {
        if (target.tag == "Box2") {
            onTriggerBox2 = false;
        }
    }
}



