﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    Transform tr_player;
    float f_RotSpeed = 3.0f, f_moveSpeed = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        tr_player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation (tr_player.position - transform.position),f_RotSpeed * Time.deltaTime);
        transform.position += transform.forward * f_moveSpeed * Time.deltaTime;
    }
}
